﻿namespace CheckItTransactionsApi.Models
{
    public class TransactionStatus : ModelBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
