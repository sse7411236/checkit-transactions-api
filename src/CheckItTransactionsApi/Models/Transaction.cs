﻿namespace CheckItTransactionsApi.Models
{
    public class Transaction
    {
        public int UserFrom { get; set; }
        public int UserTo { get; set; }
        public decimal Amount { get; set; }
        public DateTime Timestamp { get; set; }
        public int StatusId { get; set; }
        public TransactionStatus Status { get; set; }
    }
}
