﻿namespace CheckItTransactionsApi.Dtos
{
    public class BalanceDto
    {
        public int UserId { get; set; }
        public decimal Amount { get; set; }
    }
}
