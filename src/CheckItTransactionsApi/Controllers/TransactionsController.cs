using CheckItTransactionsApi.Dtos;
using CheckItTransactionsApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace CheckItTransactionsApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TransactionsController : ControllerBase
    {
        private readonly ILogger<TransactionsController> _logger;

        public TransactionsController(ILogger<TransactionsController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Transaction>> GetTransactions(int? userId = null)
        {
            return Ok();
        }

        [HttpGet("{id}")]
        public ActionResult<Transaction> GetTransaction(int id)
        {
            return Ok();
        }

        [HttpPost]
        public ActionResult<Transaction> CreateTransaction(Transaction transaction)
        {
            return Ok();
        }

        [HttpGet("balance/{userId}")]
        public ActionResult<BalanceDto> GetBalance(int userId)
        {
            return Ok();
        }
    }
}